<?php

declare(strict_types=1);

namespace BlamelessWeb\Helpers;

class Data
{
	public static function getArray($data, $include = []): array
	{
		if (is_object($data)) {
			switch (get_class($data)) {
				case 'Nette\Database\Table\Selection':
				case 'Nette\Database\Table\GroupedSelection':
					if (!empty($include)) {
						$output = [];
						foreach ($data as $item) {
							$c = self::getArray($item);
							
							foreach ($include as $type => $refs) {
								foreach ($refs as $name => $ref) {
									foreach ($ref as $table => $id) {
										if (is_numeric($name)) {
											$name = $table;
										}
										$refFn = $item->$type($table, $id);
										$c[$name] = self::getArray($refFn);
									}
								}
							}

							$output[] = $c;
						}
						return $output;
					}

					$primary = $data->getPrimary();
					$data = $data->fetchPairs($primary);
					break;
				case 'Nette\Database\Table\ActiveRow':
					return $data->toArray();
					break;
			}
		}
		return array_map(function($i) { return $i->toArray(); }, array_values($data));
	}

	public static function getObject($arr)
	{
		return (is_array($arr) ? (object) array_map(__METHOD__, $arr) : $arr);
	}

	public static function getArrayOfObjects($data, $include = []): array
	{
		$arr = is_array($data) ? $data : self::getArray($data, $include);
		$ret = [];
		foreach ($arr as $key => $value) {
			$out = $value;
			if (is_array($value) && count($value) > 0) {
				$out = self::getArrayOfObjects($value);
				if (array_key_exists(0, $value)) {
					$out = array_map(function ($i) { return (object) $i; }, $out);
				} else {
					$out = (object) $out;
				}
			}
			$ret[$key] = $out;
		}
		return $ret;
	}

	public static function mergeFromArray($object, $array)
	{
		foreach ($array as $k => $v) {
			if (is_object($object->$k) && is_array($v)) {
				$object->$k = self::mergeFromArray($object->$k, $v);
			} else if (is_null($object->$k)) {
				$object->$k = $v;
			}
		}
		return $object;
	}
}
