<?php

declare(strict_types=1);

namespace BlamelessWeb\Helpers;

class Arrays
{
	public static function getOneBy(array $data, callable $callback)
	{
		$filter = array_filter($data, $callback);
		$n = count($filter);

		if ($n > 0) {
			$filter = array_reverse($filter);
			return array_pop($filter);
		}

		return null;
	}
}
